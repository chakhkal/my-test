## Technical Writing Principles: How to Simplify Sentences

**Ask yourself the following:**

* Is this sentence easy to understand?
* Is this sentence enjoyable and easy to read?

### Cut Unnecessary Words

**Eliminate unnecessary words that don't add any additional value to the sentence.**

**Example**

This paper `provides a` reviews `of the basic tenets of` cancer biology study design, using `as` example `studies` that illustrate the `methodological` challenges `or that demonstrate successful` solutions `to the difficulties inherent in biolodical research`.[^1] 

### Other Helpful Tips for Technical Writers 

**1. Always test out the instructions yourself.**

The information may involve testing certain API calls and methods that require more advanced knowledge, and which may only be accessible under certain conditions.
Other scenarios may pose greater restrictions, such as million dollar hardware projects you can't even touch.
Regardless, it's pretty easy to tell when a technical writer has written instructions solely based on information someone else has shared rather than information the tech writer gathered from first-hand experience. Always strive to walk through the instructions and try them out yourself.

**2. Work with QA to get test cases for what you're documenting.**

QA (quality assurance) people are your best friends. They know the system better than anyone else. They've set up test environments to ensure the functionality, and they often have a set of test cases (features to try) for each release. If you're documenting an API, the QA engineers might already have the calls to test in ready-to-go formats. Piggybacking on QA efforts can make writing documentation a lot easier.

**3. Developers almost always overestimate the technical abilities of their audience.**

Developers often create products with a certain audience level in mind, and usually they assume the audience is a lot more familiar with the company's technology than they actually are. In reality, users often need more hand-holding and simple instructions than developers think. Users may resort to copying snippets of code, they may not be as familiar with the programming languages, or they may not want to devote the time to studying out your product.

**4. You can't evaluate documentation without including user feedback.**

You need to get direct feedback from customers to really know the value of the documentation. Whether users understand the documentation or not, whether it meets their needs, and so forth, is really a guessing game unless you get more tangible feedback from the intended people who actually use the help products.

**5. Always plan for collaborative authoring solutions.**

Even if you don't need collaborative authoring now, you'll most likely need it at some future point. The idea that a single person working from a single point of view can create documentation that applies to everyone in every business situation, on every platform and device, in every location and language and role, is highly impractical.

**6. Focus less on the publishing platform and more on the content.**

It's easy to get sucked into publishing details for your help tools, but that should never be your primary focus (unless you're the designated tools expert for a large team of writers who are focusing on content development). In most situations, the tool isn't the problem — it's the content. Help lives or dies by the quality of the content, not by look and design of the help. I've seen so many tech writing teams focus on improving help by focusing on the tools. Really I think tools contribute only marginally to the quality of the help experience.

**7. Balance text with visuals.**

If you want to make help look sexy, add visuals. Whether the visuals are diagrams, workflows, illustrations, or even just annotated screenshots, visuals add a lot to help material. They balance out text, provide another format for comprehension, and connect to the visual learning modes of our brains. Visuals are sometimes tedious to create, but the payoff is worth it.

**8. Examples clarify complicated concepts more than almost anything else.**

If you have something complicated, the best way to clarify it for your reader is usually through examples. Every example you add usually gives your content more and more clarity.

If you add three examples to clarify a confusing concept, your audience will probably get it in a much clearer way than if you add just one example. Granted, not everything merits an example, or even more than one example, but just remember that examples are a tool in your tech writer tool belt to communicate complex information in a way users will understand. More examples often means more clarity.

**9. Always keep learning.**

With the rapid growth of technology, technical writers must always be learning. The learning process should be a continued emphasis, one that technical writers regularly incorporate into their daily study. The resources online make learning easy. You can get more than a dozen books on nearly every technology or platform available, both written and video versions of the instructions.

**10. Your primary deliverable should be a web format.**

Many of us have been operating in the book mindset for so long we sometimes are blind to all the assumptions we bring to projects. If we target our primary deliverable as a web format, we can more appropriately write for what is often the primary way users will interact with our content.

Writing for the web means writing pages that have complete information for users to achieve a goal (what Mark Baker calls “Every page is page one”), leveraging interactivity (such as embedded tabs or filters that allow users to click tabs for code samples in different programming languages), optimizing for search features that retrieve relevant results (which might require metadata and dynamic filtering), and more. Online formats provide the possibility of online tracking, on-the-fly updates, and more. If we write for a printed, static medium (PDF) as our primary medium, we often miss all the possibilities of the web.


[^1]: *The eliminated words are highlighted* 